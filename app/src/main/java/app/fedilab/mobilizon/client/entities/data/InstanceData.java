package app.fedilab.mobilizon.client.entities.data;

/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

@SuppressWarnings({"unused", "RedundantSuppression"})
public class InstanceData {

    @SerializedName("total")
    public int total;
    @SerializedName("data")
    public List<Instance> data;


    public static class Instance {
        @SerializedName("country")
        private String country;
        @SerializedName("createdAt")
        private Date createdAt;
        @SerializedName("health")
        private int health;
        @SerializedName("host")
        private String host;
        @SerializedName("id")
        private String id;
        @SerializedName("languages")
        private List<Language> languages;
        @SerializedName("name")
        private String name;
        @SerializedName("shortDescription")
        private String shortDescription;
        @SerializedName("signupAllowed")
        private boolean signupAllowed;
        @SerializedName("supportsIPv6")
        private boolean supportsIPv6;
        @SerializedName("totalEvents")
        private int totalEvents;
        @SerializedName("totalGroups")
        private int totalGroups;
        @SerializedName("totalInstanceFollowers")
        private int totalInstanceFollowers;
        @SerializedName("totalInstanceFollowing")
        private int totalInstanceFollowing;
        @SerializedName("totalLocalEvents")
        private int totalLocalEvents;
        @SerializedName("totalLocalGroups")
        private int totalLocalGroups;
        @SerializedName("totalUsers")
        private int totalUsers;
        @SerializedName("version")
        private String version;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
        }

        public int getHealth() {
            return health;
        }

        public void setHealth(int health) {
            this.health = health;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Language> getLanguages() {
            return languages;
        }

        public void setLanguages(List<Language> languages) {
            this.languages = languages;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public boolean isSignupAllowed() {
            return signupAllowed;
        }

        public void setSignupAllowed(boolean signupAllowed) {
            this.signupAllowed = signupAllowed;
        }

        public boolean isSupportsIPv6() {
            return supportsIPv6;
        }

        public void setSupportsIPv6(boolean supportsIPv6) {
            this.supportsIPv6 = supportsIPv6;
        }

        public int getTotalEvents() {
            return totalEvents;
        }

        public void setTotalEvents(int totalEvents) {
            this.totalEvents = totalEvents;
        }

        public int getTotalGroups() {
            return totalGroups;
        }

        public void setTotalGroups(int totalGroups) {
            this.totalGroups = totalGroups;
        }

        public int getTotalInstanceFollowers() {
            return totalInstanceFollowers;
        }

        public void setTotalInstanceFollowers(int totalInstanceFollowers) {
            this.totalInstanceFollowers = totalInstanceFollowers;
        }

        public int getTotalInstanceFollowing() {
            return totalInstanceFollowing;
        }

        public void setTotalInstanceFollowing(int totalInstanceFollowing) {
            this.totalInstanceFollowing = totalInstanceFollowing;
        }

        public int getTotalLocalEvents() {
            return totalLocalEvents;
        }

        public void setTotalLocalEvents(int totalLocalEvents) {
            this.totalLocalEvents = totalLocalEvents;
        }

        public int getTotalLocalGroups() {
            return totalLocalGroups;
        }

        public void setTotalLocalGroups(int totalLocalGroups) {
            this.totalLocalGroups = totalLocalGroups;
        }

        public int getTotalUsers() {
            return totalUsers;
        }

        public void setTotalUsers(int totalUsers) {
            this.totalUsers = totalUsers;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }


    public static class Language {
        @SerializedName("displayName")
        private String displayName;
        @SerializedName("code")
        private String code;

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}

