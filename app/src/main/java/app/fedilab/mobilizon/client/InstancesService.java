package app.fedilab.mobilizon.client;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */

import app.fedilab.mobilizon.client.entities.data.InstanceData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface InstancesService {


    @GET("instances?signupAllowed=true&healthy=true")
    Call<InstanceData> getInstances(
            @Query("start") String maxId,
            @Query("count") String count,
            @Query("search") String search,
            @Query("sort") String sort);

}
