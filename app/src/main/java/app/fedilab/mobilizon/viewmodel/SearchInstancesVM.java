package app.fedilab.mobilizon.viewmodel;
/* Copyright 2020 Thomas Schneider
 *
 * This file is a part of Mobilizon app
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mobilizon app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Mobilizon app; if not,
 * see <http://www.gnu.org/licenses>. */


import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import app.fedilab.mobilizon.client.RetrofitInstancesAPI;
import app.fedilab.mobilizon.client.entities.Search;
import app.fedilab.mobilizon.client.entities.data.InstanceData;

public class SearchInstancesVM extends ViewModel {

    private MutableLiveData<InstanceData> instanceDataMutableLiveData;


    public LiveData<InstanceData> get(Search search) {
        instanceDataMutableLiveData = new MutableLiveData<>();
        searchInstances(search);
        return instanceDataMutableLiveData;
    }


    private void searchInstances(Search search) {
        new Thread(() -> {
            try {
                RetrofitInstancesAPI retrofitInstancesAPI = new RetrofitInstancesAPI();
                InstanceData instanceData = retrofitInstancesAPI.search(search);
                Handler mainHandler = new Handler(Looper.getMainLooper());
                Runnable myRunnable = () -> instanceDataMutableLiveData.setValue(instanceData);
                mainHandler.post(myRunnable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

}
