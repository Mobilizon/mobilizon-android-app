# Mobilizon app [![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
A simple client to interact with Mobilizon instances

Currently the app works with a webview. It supports fullscreen for videos and location. You can switch to another instance. Its validity will be checked with node info.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/app.fedilab.mobilizon/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=app.fedilab.mobilizon)